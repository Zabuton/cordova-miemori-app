var app = angular.module('my-app', ['onsen.directives', 'ngSanitize', 'wc.Directives','ngPinchZoom']);
app.controller('AppController', function($scope) {
    console.log('App started.');
});
ons.ready(function(){
    console.log('onsenUI is ready.');
});


// ピックアップタブ
app.controller('PickupController', function($scope, Events, Buffer) {
    // 表示
    $scope.loads = true;
    Events.getEventsAvailable().then(function(items){
        if (!items.error) {
            $scope.items = items;
            $scope.collectionWidth = items.length * 225;
            $scope.loads = false;
        } else {
            $scope.msg = items.error;
        }
    });
    // クリック
    $scope.showEventDetail = function(cellno){
        Buffer.cellno = cellno;
        Buffer.from = 'pickup';
        console.log('selectedCellNo: '+Buffer.cellno);
        mainNavigator.pushPage('event-detail.html');
    }    
});

// イベントタブ
app.controller('EventsController', function($scope, Events, Buffer) {
    // 表示
    Events.getEvents().then(function(items){
        $scope.items = items;
        $scope.collectionWidth = items.length * 225;
    });
    // クリック
    $scope.showEventDetail = function(cellno){
        Buffer.cellno = cellno;
        Buffer.from = 'eventlist';
        console.log('selectedCellNo: '+Buffer.cellno);
        eventsNavigator.pushPage('event-detail.html');
    }
});

// イベント詳細ページ
app.controller('DetailController', function($scope, Events, Buffer) {
    if(Buffer.from=='eventlist') {
        Events.getEvents().then(function(item){
            $scope.item = item[Buffer.cellno];
            $scope.entry = item[Buffer.cellno].entry_status==1 ? true : false;
        });
    } else {
        Events.getEventsAvailable().then(function(item){
            $scope.item = item[Buffer.cellno]
            $scope.entry = true;
        });
    }
});

// イベント申込みポップオーバー
app.controller('PostentryController', function($scope, $http, Config, Events, Buffer) {
    var posturl = 'http://mie-mori.jp/api/evententry/';
    var result = {};
    $scope.postEntry = function(){
        $http({
            method: 'POST',
            url: posturl
        // 取得成功
        }).then(function successCallback(response) {
            result = response.data;
            console.log(response.data);
            alert(Config.entryDoneMsg);
            return result;
        //取得失敗
        }, function errorCallback(response) {
            alert(Config.entryErrMsg+'('+response.error+')');
            return false;
        });
        $scope.EventEntry.hide()
    }
});

// 園内マップタブ
app.controller('ParkMapsController', function($scope, $timeout, $interval, GoogleMaps) {
    // マップの種類 -> true:IllustMap, false:GoogleMaps
    $scope.maps = false;
    
    // ステータスを更新
    $interval(function(){
        $scope.msg = GoogleMaps.message;
    },1000);
    
    // タブがクリックされたらGoogleMapsを再描画
    tab.on('postchange', function(event) {
        
        if(event.index==2 && GoogleMaps.map)
            google.maps.event.trigger(GoogleMaps.map, 'resize');
    });
    // 切り替えボタン
    $scope.ChoseGooglemap = function() {
        console.log('IllustMap is chosen.');
        $scope.maps = true;
        google.maps.event.trigger(GoogleMaps.map, 'resize');
    }
    $scope.ChoseIllustmap = function() {
        console.log('GoogleMaps is chosen.');
        $scope.maps = false;
    }
    
});

/*******************************************************************/
/*******************                         ***********************/
/*******************         FILTERS         ***********************/
/*******************                         ***********************/
/*******************************************************************/

// 改行コード変換
app.filter('nl2br', function(){
    return function(value){
        if(!angular.isString(value)){
            return value;
        }
        return value.replace(/(\r\n|\n|\r)/g, '<br />');
    }
});

/*******************************************************************/
/*******************                         ***********************/
/*******************        FUNCTIONS        ***********************/
/*******************                         ***********************/
/*******************************************************************/




/*******************************************************************/
/*******************                         ***********************/
/*******************          MODEL          ***********************/
/*******************                         ***********************/
/*******************************************************************/

// イベント詳細データ
app.factory('Events', function($http){
    var items = {};
    return {
        getEvents: function(){
            var geturl = 'http://mie-mori.jp/api/eventlist/';
            // 通信開始
            return $http({
                    method: 'GET',
                    url: geturl,
                    cache: true
                // 取得成功
                }).then(function successCallback(response) {
                    items = response.data;
                    //console.log(response.data);
                    return items;
                //取得失敗
                }, function errorCallback(response) {
                    console.log('データ読み込みエラー:'+response.error);
                    items.error = 'データが読込みできませんでした。';
                    return items;
                });
        },
        getEventsAvailable: function(){
            var geturl = 'http://mie-mori.jp/api/eventlist/reception/';
            // 通信開始
            return $http({
                    method: 'GET',
                    url: geturl,
                    cache: true
                // 取得成功
                }).then(function successCallback(response) {
                    items = response.data;
                    //console.log(response.data);
                    return items;
                //取得失敗
                }, function errorCallback(response) {
                    console.log('データ読み込みエラー:'+response.error);
                    items.error = 'データが読込みできませんでした。';
                    return items;
                });
        }
    }
});
// GoogleMaps
app.service('GoogleMaps', function($timeout, $interval){
    var data = {};
    data.message = "";
    
    // マップ初期化
    $timeout(function(){

        var latlng = new google.maps.LatLng(35.046705,136.478759);
        var myOptions = {
            zoom: 19,
            center: latlng,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.SATELLITE
        };
        data.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 
        data.overlay = new google.maps.OverlayView();
        data.overlay.draw = function() {};
        data.overlay.setMap(data.map);
        data.element = document.getElementById('map_canvas');

        // 自然学習展示館
        data.marker = new google.maps.Marker({
            position: new google.maps.LatLng(35.0467, 136.4788),
            map: data.map,
            label: {text:'自然学習展示館', color:'white'}
        });
        // 森の図書館
        data.marker = new google.maps.Marker({
            position: new google.maps.LatLng(35.04657, 136.47873),
            map: data.map,
            label: {text:'森の図書館', color:'white'}
        });
        // 芝生広場
        data.marker = new google.maps.Marker({
            position: new google.maps.LatLng(35.0464, 136.4782),
            map: data.map,
            label: {text:'芝生広場', color:'white'}
        });
        
        // 現在地を取得
        var lat,lon;
        data.currentOn = false;
        data.current = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lon),
            map: data.map
        });
        $interval(function(){
            if (navigator.geolocation) {
                // 現在の位置情報取得を実施
                navigator.geolocation.getCurrentPosition(function (pos) {
                    // 位置情報取得成功時
                    lat = pos.coords.latitude;
                    lon = pos.coords.longitude;
                    acc = pos.coords.accuracy;
                   
                    var current_pos = new google.maps.LatLng(lat, lon);
                   
                   if(data.currentOn!=true) {
                       data.currentOn = true;
                       // 現在地をセット
                       data.current.setPosition(new google.maps.LatLng(lat, lon));
                       
                        // 誤差を円で描く
                       data.circle = new google.maps.Circle({
                            map: data.map,
                            center: current_pos,
                            radius: acc, // 単位はメートル
                            strokeColor: '#0088ff',
                            strokeOpacity: 0.8,
                            strokeWeight: 1,
                            fillColor: '#0088ff',
                            fillOpacity: 0.2
                       });

                   } else {
                       data.current.setPosition(new google.maps.LatLng(lat, lon));
                       data.circle.setCenter(new google.maps.LatLng(lat, lon));
                       // 現在地にスクロールさせる
                       data.map.panTo(current_pos);
                   }

                   //data.message = "位置情報を取得中";

               },
               // 位置情報取得失敗時
               function (error) {
                    switch (error.code) {
                       // 位置情報取得できない場合
                      case error.POSITION_UNAVAILABLE:
                           data.message = "位置情報の取得ができませんでした。";
                            break;
                     // Geolocation使用許可されない場合
                     case error.PERMISSION_DENIED:
                           data.message = "位置情報取得の使用許可がされませんでした。";
                           break;
                            // タイムアウトした場合 
                            case error.PERMISSION_DENIED_TIMEOUT:
                            data.message = "位置情報取得中にタイムアウトしました。";
                            break;
                    }
                });
            } else {
                data.message = "Geolocationが使えません。";
            }
            return data;
        },1000);
        return data;
    },100);
    return data;
});

// コントローラー間の受け渡しデータ
app.service('Buffer', function(){
    var data = {};
    data.cellno = 0;
    data.from = '';
    return data;
});

// config
app.value('Config', {
    entryDoneMsg: 'お申込み頂きありがとうございます! 担当から折り返しご連絡いたします。',
    entryErrMsg: '送信できませんでした。'
});